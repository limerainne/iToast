package win.limerainne.itoast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

// show toast from ADB
// https://stackoverflow.com/questions/22634446/sending-intent-to-broadcastreceiver-from-adb?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
// > adb shell am broadcast -a win.limerainne.itoast.toast -n win.limerainne.itoast/.ToastReceiver --es msg 'Test 앱' win.limerainne.itoast

class ToastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        previousToast?.cancel()

        val message: String = intent.getStringExtra(TagMessage)?.replace("\\n", "\n") ?: intent.action ?: ""

        // NOTE cannot set custom toast duration
        previousToast = Toast.makeText(context, message, Toast.LENGTH_LONG)?.apply {
            show()
        }
    }

    companion object {
        private var previousToast: Toast? = null

        val TagMessage = "msg"
    }
}