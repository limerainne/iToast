package win.limerainne.itoast

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.content.Context.POWER_SERVICE
import android.content.Intent
import android.os.Build
import android.os.PowerManager



class DialogActivity: Activity() {
    lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        val title: String?
        val message: String
        val nonDissmissable: Boolean

        val intent = getIntent()
        title = intent.getStringExtra(DialogReceiver.TagTitle)
        message = intent.getStringExtra(DialogReceiver.TagMessage)

        nonDissmissable = intent.getBooleanExtra(DialogReceiver.TagNonDissmiss, false)


        dialog = AlertDialog.Builder(this).let {
            it.setTitle(title ?: getString(R.string.app_name))
            it.setMessage(message)
            it.setPositiveButton(android.R.string.ok) {_, _ -> this.finish()}

            if (intent.action == Intent.ACTION_DEVICE_STORAGE_LOW)
                it.setIcon(android.R.drawable.ic_popup_disk_full)

            if (nonDissmissable)
                it.setCancelable(false)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                it.setOnDismissListener {
                    this.finish()
                }
            }

            it.setOnCancelListener {
                this.finish()
            }

            it.create()
        }.apply {
            show()
        }
    }

    override fun onPause() {
        super.onPause()

        dialog.dismiss()
    }
}