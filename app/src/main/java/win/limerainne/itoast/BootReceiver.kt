package win.limerainne.itoast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

class BootReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action != Intent.ACTION_BOOT_COMPLETED)
            return

        // NOTE use 'applicationContext'; ref: Stackoverflow/37787291
        context.applicationContext.apply {
            val storageLowIntentFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val storageOKIntentFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_OK)

            val receiver = DialogReceiver()
            registerReceiver(receiver, storageLowIntentFilter)
            registerReceiver(receiver, storageOKIntentFilter)
        }
    }
}