package win.limerainne.itoast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.PowerManager


// show dialog from ADB
// https://stackoverflow.com/questions/22634446/sending-intent-to-broadcastreceiver-from-adb?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
// > adb shell am broadcast -a win.limerainne.itoast.dialog -n win.limerainne.itoast/.DialogReceiver --es msg 'Test 앱' --ez reboot true win.limerainne.itoast

class DialogReceiver: BroadcastReceiver() {
    private lateinit var wakelock: PowerManager.WakeLock

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            actionTag -> {
                openDialog(context, intent)
            }
            Intent.ACTION_DEVICE_STORAGE_LOW -> {
                acquireWakelock(context)
                sleepFor()
                openStorageLowWarningDialog(context)
                releaseWakelock()
            }
            Intent.ACTION_DEVICE_STORAGE_OK -> {
                acquireWakelock(context)
                sleepFor()
                openStorageOKDialog(context)
                releaseWakelock()
            }
        }
    }

    private fun openDialog(context: Context, intent: Intent) {
        val title: String? = intent.getStringExtra(TagTitle) ?: intent.action
        val message: String = intent.getStringExtra(ToastReceiver.TagMessage)?.replace("\\n", "\n")
                ?: ""

        val i = Intent(context.applicationContext, DialogActivity::class.java).apply {
            //            putExtras(intent.extras)
            putExtra(TagTitle, title)
            putExtra(TagMessage, message)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        context.startActivity(i)
    }

    private fun openStorageLowWarningDialog(context: Context) {
        val i = Intent(context.applicationContext, DialogActivity::class.java).apply {
            action = Intent.ACTION_DEVICE_STORAGE_LOW
            putExtra(TagTitle, context.getString(R.string.storage_low_title))
            putExtra(TagMessage, context.getString(R.string.storage_low_desc))
            putExtra(TagNonDissmiss, true)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        context.startActivity(i)
    }

    private fun openStorageOKDialog(context: Context) {
        val i = Intent(context.applicationContext, DialogActivity::class.java).apply {
            putExtra(TagTitle, context.getString(R.string.storage_ok_title))
            putExtra(TagMessage, context.getString(R.string.storage_ok_desc))
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        context.startActivity(i)
    }

    private fun acquireWakelock(context: Context, timeout: Long = 5*1000) {
        // in sleep mode, receiver can't start an activity
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        wakelock = pm.newWakeLock(
                PowerManager.SCREEN_DIM_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE,
                "iToast:LowStorageWakelock"
        )
        wakelock.acquire(timeout)
    }

    private fun sleepFor(time_ms: Long = 1000) {
        Thread.sleep(time_ms)
    }

    private fun releaseWakelock() {
        wakelock.release()
    }

    companion object {
        val actionTag = "win.limerainne.itoast.dialog"

        val TagTitle = "title"
        val TagMessage = "msg"

        val TagNonDissmiss = "nonDismiss"
    }
}